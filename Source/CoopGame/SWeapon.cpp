// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeapon.h"
#include "CoopGame.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(
    TEXT( "COOP.DebugWeapons" ),
    DebugWeaponDrawing,
    TEXT( "Draw Debug Lines for Weapons" ),
    ECVF_Cheat
);

// Sets default values
ASWeapon::ASWeapon()
{
    MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>( TEXT( "MeshComp" ) );
    RootComponent = MeshComp;

    MuzzleSocketName = "MuzzleSocket";
    TracerTargetName = "Target";

    BaseDamage = 20.0f;
    RateOfFire = 600;
    ClipCapacity = 30;
    TimeBetweenReloads = 2.167f;
    bIsReloading = false;

    SetReplicates( true );

    NetUpdateFrequency = 66.0f;
    MinNetUpdateFrequency = 33.0f;

    BulletSpread = 1.0f;
}

void ASWeapon::BeginPlay()
{
    Super::BeginPlay();

    TimeBetweenShots = 60 / RateOfFire;
    BulletsInClip = ClipCapacity;
}

void ASWeapon::Fire()
{
    if ( Role < ROLE_Authority )
    {
        ServerFire();
    }

    AActor *MyOwner = GetOwner();

    if ( MyOwner )
    {
        if ( BulletsInClip > 0 && !bIsReloading )
        {
            BulletsInClip--;

            FVector EyeLocation;
            FRotator EyeRotation;
            MyOwner->GetActorEyesViewPoint( EyeLocation, EyeRotation );

            FVector ShotDirection = EyeRotation.Vector();

            float HalfRad = FMath::DegreesToRadians( BulletSpread );

            ShotDirection  = FMath::VRandCone( ShotDirection, HalfRad, HalfRad );

            FVector TraceEnd = EyeLocation + ( ShotDirection * 10000 );

            FCollisionQueryParams QueryParams;
            QueryParams.AddIgnoredActor( MyOwner );
            QueryParams.AddIgnoredActor( this );
            QueryParams.bTraceComplex = true;
            QueryParams.bReturnPhysicalMaterial = true;

            //Particle "Target" parameter
            FVector TracerEndPoint = TraceEnd;

            EPhysicalSurface SurfaceType = SurfaceType_Default;
            FHitResult Hit;
            if ( GetWorld()->LineTraceSingleByChannel(
                     Hit,
                     EyeLocation,
                     TraceEnd,
                     COLLISION_WEAPON,
                     QueryParams
                 ) )
            {
                AActor *HitActor = Hit.GetActor();

                SurfaceType = UPhysicalMaterial::DetermineSurfaceType( Hit.PhysMaterial.Get() );

                float ActualDamage = BaseDamage;

                if ( SurfaceType == SURFACE_FLESHVULNERABLE )
                {
                    ActualDamage *= 4.0f;
                }

                UGameplayStatics::ApplyPointDamage(
                    HitActor,
                    ActualDamage,
                    ShotDirection,
                    Hit,
                    MyOwner->GetInstigatorController(),
                    MyOwner,
                    DamageType
                );

                if ( DefaultImpactEffect )
                {
                    UGameplayStatics::SpawnEmitterAtLocation(
                        GetWorld(),
                        DefaultImpactEffect,
                        Hit.ImpactPoint,
                        Hit.ImpactNormal.Rotation()
                    );
                }

                PlayImpactEffects( SurfaceType, Hit.ImpactPoint );

                TracerEndPoint = Hit.ImpactPoint;
            }

            if ( DebugWeaponDrawing > 0 )
            {
                DrawDebugLine( GetWorld(), EyeLocation, TraceEnd, FColor::White, 1.0f, 1.0f );
            }

            PlayFireEffects( TracerEndPoint );

            if ( Role == ROLE_Authority )
            {
                HitScanTrace.TraceTo = TracerEndPoint;
                HitScanTrace.SurfaceType = SurfaceType;
            }

            LastFiredTime = GetWorld()->TimeSeconds;
        }
    }
}

void ASWeapon::ServerFire_Implementation()
{
    Fire();
}

bool ASWeapon::ServerFire_Validate()
{
    return true;
}

void ASWeapon::StartFire()
{
    float FirstDelay = FMath::Max( LastFiredTime + TimeBetweenShots - GetWorld()->TimeSeconds, 0.0f );

    GetWorldTimerManager().SetTimer( TimerHandle_TimeBetweenShots, this, &ASWeapon::Fire,
                                     TimeBetweenShots, true, FirstDelay );
}

void ASWeapon::StopFire()
{
    GetWorldTimerManager().ClearTimer( TimerHandle_TimeBetweenShots );
}

void ASWeapon::StartReload()
{
    if ( Role < ROLE_Authority )
    {
        ServerReload();
    }

    if ( !GetWorldTimerManager().IsTimerActive( TimerHandle_TimeBetweenReloads ) )
    {
        bIsReloading = true;
        GetWorldTimerManager().SetTimer( TimerHandle_TimeBetweenReloads, this, &ASWeapon::Reload,
                                         TimeBetweenReloads, false );
    }
}

void ASWeapon::Reload()
{
    BulletsInClip = ClipCapacity;
    bIsReloading = false;
}

void ASWeapon::ServerReload_Implementation()
{
    StartReload();
}

bool ASWeapon::ServerReload_Validate()
{
    return true;
}

void ASWeapon::OnRep_HitScanTrace()
{
    // Play cosmetic FX
    PlayFireEffects( HitScanTrace.TraceTo );
    PlayImpactEffects( HitScanTrace.SurfaceType, HitScanTrace.TraceTo );
}

void ASWeapon::PlayFireEffects( FVector TraceEnd )
{
    if ( MuzzleEffect )
    {
        UGameplayStatics::SpawnEmitterAttached( MuzzleEffect, MeshComp, MuzzleSocketName );
    }

    if ( TracerEffect )
    {
        FVector MuzzleLocation = MeshComp->GetSocketLocation( "MuzzleSocket" );

        UParticleSystemComponent *TracerComp = UGameplayStatics::SpawnEmitterAtLocation(
                                                   GetWorld(),
                                                   TracerEffect,
                                                   MuzzleLocation
                                               );

        if ( TracerComp )
        {
            TracerComp->SetVectorParameter( "Target", TraceEnd );
        }
    }

    APawn *MyOwner = Cast<APawn>( GetOwner() );

    if ( MyOwner )
    {
        APlayerController *PC = Cast<APlayerController>( MyOwner->GetController() );

        if ( PC )
        {
            PC->ClientPlayCameraShake( FireCamShake );
        }
    }
}

void ASWeapon::PlayImpactEffects( EPhysicalSurface SurfaceType, FVector ImpactPoint )
{
    UParticleSystem *SelectedEffect = nullptr;

    switch ( SurfaceType )
    {
        case SURFACE_FLESHDEFAULT:
        case SURFACE_FLESHVULNERABLE:
            SelectedEffect = FleshImpactEffect;
            break;
        default:
            SelectedEffect = DefaultImpactEffect;
            break;
    }

    if ( SelectedEffect )
    {
        FVector MuzzleLocation = MeshComp->GetSocketLocation( MuzzleSocketName );

        FVector ShotDirection = ImpactPoint - MuzzleLocation;
        ShotDirection.Normalize();

        UGameplayStatics::SpawnEmitterAtLocation(
            GetWorld(),
            SelectedEffect,
            ImpactPoint,
            ShotDirection.Rotation()
        );
    }
}

void ASWeapon::GetLifetimeReplicatedProps( TArray<FLifetimeProperty> &OutLifetimeProps ) const
{
    Super::GetLifetimeReplicatedProps( OutLifetimeProps );

    DOREPLIFETIME_CONDITION( ASWeapon, HitScanTrace, COND_SkipOwner );
    DOREPLIFETIME_CONDITION( ASWeapon, bIsReloading, COND_SkipOwner );
}