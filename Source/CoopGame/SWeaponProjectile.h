// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SWeaponProjectile.generated.h"

class UProjectileMovementComponent;
class USphereComponent;
class UStaticMeshComponent;
class UParticleSystem;

UCLASS()
class COOPGAME_API ASWeaponProjectile : public AActor
{
        GENERATED_BODY()

    public:
        // Sets default values for this actor's properties
        ASWeaponProjectile();

    protected:
        // Called when the game starts or when spawned
        virtual void BeginPlay() override;

        /** Sphere collision component */
        UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Projectile" )
        USphereComponent * CollisionComp;

        /** Sphere mesh component */
        UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Projectile" )
        UStaticMeshComponent * MeshComp;

        /** Projectile movement component */
        UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "Movement" )
        UProjectileMovementComponent * ProjectileMovement;

        UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, Category = "Projectile" )
        TSubclassOf<UDamageType> DamageType;

        UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, Category = "Particles" )
        UParticleSystem * ExplosionEffect;

        UFUNCTION()
        void Explode();

    public:
        // Called every frame
        virtual void Tick( float DeltaTime ) override;

};
