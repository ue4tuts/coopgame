// Fill out your copyright notice in the Description page of Project Settings.

#include "SExplosiveBarrel.h"
#include "Components/SHealthComponent.h"
#include "Components/StaticMeshComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASExplosiveBarrel::ASExplosiveBarrel()
{
    MeshComp = CreateDefaultSubobject<UStaticMeshComponent>( TEXT( "MeshComp" ) );
    MeshComp->SetCollisionObjectType( ECollisionChannel::ECC_PhysicsBody );
    MeshComp->SetSimulatePhysics( true );
    RootComponent = MeshComp;

    HealthComp = CreateDefaultSubobject<USHealthComponent>( TEXT( "HealthComp" ) );

    RadialForceComp = CreateDefaultSubobject<URadialForceComponent>( TEXT( "RadialComp" ) );
    RadialForceComp->Radius = 300.0f;
    RadialForceComp->bImpulseVelChange = true;
    RadialForceComp->bIgnoreOwningActor = true;
    RadialForceComp->bAutoActivate = false;
    RadialForceComp->SetupAttachment( MeshComp );

    bExploded = false;
    SetReplicates( true );
    SetReplicateMovement( true );
    NetUpdateFrequency = 66.0f;
    MinNetUpdateFrequency = 33.0f;
}

void ASExplosiveBarrel::BeginPlay()
{
    Super::BeginPlay();

    HealthComp->OnHealthChanged.AddDynamic( this, &ASExplosiveBarrel::Explode );
}

void ASExplosiveBarrel::Explode(
    USHealthComponent *HealthComponent,
    float Health,
    float HealthDelta,
    const UDamageType *DamageType,
    AController *InstigatedBy,
    AActor *DamageCauser
)
{
    if ( Health <= 0 && !bExploded )
    {
        if ( Role = ROLE_Authority )
        {
            bExploded = true;

            OnRep_Exploded();

            MeshComp->AddImpulse( FVector( 0.0f, 0.0f, 1000.0f ), NAME_None, true );

            RadialForceComp->FireImpulse();
        }
    }
}

void ASExplosiveBarrel::OnRep_Exploded()
{
    if ( ExplosionEffect )
    {
        UGameplayStatics::SpawnEmitterAtLocation( GetWorld(), ExplosionEffect, GetActorLocation() );
    }

    MeshComp->SetMaterial( 0, ExplodedMaterial );
}

void ASExplosiveBarrel::GetLifetimeReplicatedProps( TArray<FLifetimeProperty> &OutLifetimeProps )
const
{
    Super::GetLifetimeReplicatedProps( OutLifetimeProps );

    DOREPLIFETIME( ASExplosiveBarrel, bExploded );
}