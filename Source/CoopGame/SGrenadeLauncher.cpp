// Fill out your copyright notice in the Description page of Project Settings.

#include "SGrenadeLauncher.h"
#include "SWeaponProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Components/SkeletalMeshComponent.h"

void ASGrenadeLauncher::Fire()
{
    AActor *MyOwner = GetOwner();

    if ( MyOwner && ProjectileClass )
    {
        FVector EyeLocation;
        FRotator EyeRotation;
        MyOwner->GetActorEyesViewPoint( EyeLocation, EyeRotation );

        if ( ProjectileClass )
        {
            FVector MuzzleLocation = MeshComp->GetSocketLocation( MuzzleSocketName );

            FActorSpawnParameters ActorSpawnParams;
            ActorSpawnParams.SpawnCollisionHandlingOverride =
                ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

            GetWorld()->SpawnActor<ASWeaponProjectile>( ProjectileClass, MuzzleLocation, EyeRotation,
                                                        ActorSpawnParams );
        }
    }
}