// Fill out your copyright notice in the Description page of Project Settings.

#include "SPickupActor.h"
#include "SPowerUpActor.h"
#include "SCharacter.h"
#include "Components/SphereComponent.h"
#include "Components/DecalComponent.h"
#include "TimerManager.h"

// Sets default values
ASPickupActor::ASPickupActor()
{
    SphereComp = CreateDefaultSubobject<USphereComponent>( TEXT( "SphereComp" ) );
    SphereComp->SetSphereRadius( 75.0f );
    RootComponent = SphereComp;

    DecalComp = CreateDefaultSubobject<UDecalComponent>( TEXT( "DecalComp" ) );
    DecalComp->SetRelativeRotation( FRotator( 90.0f, 0.0f, 0.0f ) );
    DecalComp->DecalSize = FVector( 64, 75, 75 );
    DecalComp->SetupAttachment( RootComponent );

    CooldownDuration = 10.0f;

    SetReplicates( true );
}

// Called when the game starts or when spawned
void ASPickupActor::BeginPlay()
{
    Super::BeginPlay();

    if ( Role == ROLE_Authority )
    {
        Respawn();
    }
}

void ASPickupActor::Respawn()
{
    if ( PowerUpClass )
    {
        FActorSpawnParameters SpawnParams;
        SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

        PowerUpInstance = GetWorld()->SpawnActor<ASPowerUpActor>( PowerUpClass, GetActorTransform(),
                          SpawnParams );
    }
    else
    {
        UE_LOG( LogTemp, Warning, TEXT( "PowerUpClass is nullptr in %s. Please update your Blueprint." ),
                *GetName() );
    }
}

void ASPickupActor::NotifyActorBeginOverlap( AActor *OtherActor )
{
    Super::NotifyActorBeginOverlap( OtherActor );

    if ( Role == ROLE_Authority && PowerUpInstance )
    {
        ASCharacter *PlayerPawn = Cast<ASCharacter>( OtherActor );

        if ( PlayerPawn )
        {
            PowerUpInstance->ActivatePowerUp( OtherActor );
            PowerUpInstance = nullptr;

            GetWorldTimerManager().SetTimer( TimerHandle_RespawnTimer, this, &ASPickupActor::Respawn,
                                             CooldownDuration );
        }
    }
}

