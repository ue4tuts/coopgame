// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SWeapon.h"
#include "SGrenadeLauncher.generated.h"

class ASWeaponProjectile;

/**
 *
 */
UCLASS()
class COOPGAME_API ASGrenadeLauncher : public ASWeapon
{
        GENERATED_BODY()

    protected:
        virtual void Fire() override;

        UPROPERTY( EditDefaultsOnly, Category = "Projectile" )
        TSubclassOf<ASWeaponProjectile> ProjectileClass;
};
