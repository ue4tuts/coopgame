// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SExplosiveBarrel.generated.h"

class USHealthComponent;
class URadialForceComponent;
class UParticleSystem;

UCLASS()
class COOPGAME_API ASExplosiveBarrel : public AActor
{
        GENERATED_BODY()

    public:
        // Sets default values for this actor's properties
        ASExplosiveBarrel();

    protected:
        virtual void BeginPlay() override;

        UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "ExplosiveBarrel" )
        UStaticMeshComponent * MeshComp;

        UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "ExplosiveBarrel" )
        USHealthComponent * HealthComp;

        UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = "ExplosiveBarrel" )
        URadialForceComponent * RadialForceComp;

        UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, Category = "ExplosiveBarrel" )
        UParticleSystem * ExplosionEffect;

        UPROPERTY( EditDefaultsOnly, BlueprintReadOnly, Category = "ExplosiveBarrel" )
        UMaterialInterface * ExplodedMaterial;

        UFUNCTION()
        void Explode(
            USHealthComponent *HealthComponent,
            float Health,
            float HealthDelta,
            const class UDamageType *DamageType,
            class AController *InstigatedBy,
            AActor *DamageCauser
        );

        UPROPERTY( ReplicatedUsing = OnRep_Exploded )
        bool bExploded;

        UFUNCTION()
        void OnRep_Exploded();
};
