// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeaponProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
ASWeaponProjectile::ASWeaponProjectile()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    // Use a sphere as a simple collision representation
    CollisionComp = CreateDefaultSubobject<USphereComponent>( TEXT( "SphereComp" ) );
    CollisionComp->InitSphereRadius( 5.0f );
    CollisionComp->SetCollisionResponseToAllChannels( ECollisionResponse::ECR_Ignore );
    CollisionComp->SetCollisionResponseToChannel( ECollisionChannel::ECC_Pawn,
                                                  ECollisionResponse::ECR_Block );
    CollisionComp->SetCollisionResponseToChannel( ECollisionChannel::ECC_WorldStatic,
                                                  ECollisionResponse::ECR_Block );

    // Players can't walk on it
    CollisionComp->SetWalkableSlopeOverride( FWalkableSlopeOverride( WalkableSlope_Unwalkable, 0.f ) );
    CollisionComp->CanCharacterStepUpOn = ECB_No;

    // Set as root component
    RootComponent = CollisionComp;

    MeshComp = CreateDefaultSubobject<UStaticMeshComponent>( TEXT( "MeshComp" ) );
    MeshComp->SetupAttachment( CollisionComp );
    MeshComp->SetCollisionEnabled( ECollisionEnabled::NoCollision );

    // Use a ProjectileMovementComponent to govern this projectile's movement
    ProjectileMovement =
        CreateDefaultSubobject<UProjectileMovementComponent>( TEXT( "ProjectileComp" ) );
    ProjectileMovement->UpdatedComponent = CollisionComp;
    ProjectileMovement->InitialSpeed = 1000.f;
    ProjectileMovement->MaxSpeed = 3000.f;
    ProjectileMovement->bRotationFollowsVelocity = true;
    ProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void ASWeaponProjectile::BeginPlay()
{
    Super::BeginPlay();

    FTimerHandle TimerHandle;

    GetWorldTimerManager().SetTimer(
        TimerHandle,
        this,
        &ASWeaponProjectile::Explode,
        1.0f,
        false
    );
}

void ASWeaponProjectile::Explode()
{
    TArray<AActor *> IgnoreActors;

    if ( UGameplayStatics::ApplyRadialDamage(
             GetWorld(),
             50.0f,
             GetActorLocation(),
             80.0f,
             DamageType,
             IgnoreActors,
             this,
             GetInstigatorController(),
             true
         ) )
    {

    }

    DrawDebugSphere(
        GetWorld(),
        GetActorLocation(),
        80.0f,
        16,
        FColor::Red,
        false,
        2.0f
    );

    if ( ExplosionEffect )
    {
        UGameplayStatics::SpawnEmitterAtLocation(
            GetWorld(),
            ExplosionEffect,
            GetActorLocation(),
            GetActorRotation()
        );
    }

    Destroy();
}

// Called every frame
void ASWeaponProjectile::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );

}

